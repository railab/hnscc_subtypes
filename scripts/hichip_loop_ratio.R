#####################################################################
# Author: Carson Callahan
# Date: 2021-07-06
# 
# Program Use:
# 1. Compute loop/enhancer ratios
#
# Notes:
# 1. Same anchors were used for each sample -- from hichip peaks
#####################################################################


# --------------- #
#### libraries ####
# --------------- #
library(here)
library(tidyverse)


# --------------------------- #
#### read loop counts file ####
# --------------------------- #

# these are filtered, intrachromosomal loops from hichipper

loops <- read.table(file = "/data/hichip/loop_counts.txt",
                    header = FALSE)


# -------------------------- #
#### fix names and divide ####
# -------------------------- #

colnames(loops) <- c("loop_counts", "sample")

loops$sample <- gsub(pattern = "(.*)\\.filt\\.intra\\.loop_counts\\.bedpe", replacement = "\\1", loops$sample)
loops$sample <- gsub(pattern = "HNSC-(.*)", replacement = "\\1", loops$sample)

loops$ratio <- round(loops$loop_counts/44049, digits = 2)


# ---------------------------------- #
#### make plots for visualization ####
# ---------------------------------- #

loops$subtype <- c("Classical", "Basal", "Atypical", "Mesenchmyal")

ggplot(data = loops, aes(x = subtype, y = ratio)) +
  geom_segment(aes(x = subtype, xend = subtype, y = 0, yend = ratio), lwd = 1.5) +
  geom_point(size = 8, color = c("#228BDC", "#D98594", "#EB4762", "#D96237")) + 
  scale_color_manual(values = c("#228BDC", "#D98594", "#EB4762", "#D96237")) +
  scale_y_continuous(expand = c(0,0), limits = (c(0,11))) +
  scale_x_discrete(expand = c(0.2, 0)) +
  coord_flip() +
  theme(
    plot.title = element_text(hjust=0.5, face = "bold", size = 20),
    axis.title.x = element_text(size = 15),
    axis.text.x = element_text(size = 12, angle = 45, hjust = 1),
    axis.title.y = element_text(size = 15),
    axis.text.y = element_text(size = 10),
    legend.title = element_text(size = 12),
    legend.text = element_text(size = 12),
    panel.background = element_blank(), axis.line = element_line(color = "black", size = 1),
    panel.grid.major.x = element_line(colour = "grey"))
ggsave(filename = here('outputs/figures/molec_subtypes_hichip_loop_ratio.png'),
       dpi = 600, width = 8, height = 8)