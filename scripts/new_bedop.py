#!/usr/bin/env python3
import glob
import os
import operator

## Author: Jonathan Schulz

def write_bed(file_name, to_write):
	with open(file_name, 'w') as f:
		f.writelines('\t'.join(i) + '\n' for i in to_write)

#this is the main function, look in here for the setting for the file name
def merge_count(bed_dir, intermediate_filename, final_output, dont_touch, save_intermediate_file):
	# print('hi')
	# print(list(glob.iglob(os.path.join(bed_dir, '*'))))
	bed_count = 0
	bed_name_list = []
	list_peak_lists = []
	for filepath in glob.iglob(os.path.join(bed_dir, '*.bed')):
		if filepath.split('/')[-1] not in dont_touch:
			bed_count += 1
			# print(filepath)
			# continue
			#this is where it decides which part of the filename to keep
			#.split('/')[-1] cuts the path off of the filename, and .split('_') splits the filename on '_'. The [0] takes the string before the first '_'
			#If the string you want is after the first '_', use [1]
			bed_name_list.append(filepath.split('/')[-1].split('_')[0])
			this_peak_list = []
			with open(filepath, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					this_peak_list.append(line)
			list_peak_lists.append(this_peak_list)
	# exit()
	file_lens = [len(list_peak_lists[thing]) for thing in range(len(list_peak_lists))]
	sorted_array = [(r,x,f) for r,x,f in sorted(zip(file_lens,bed_name_list,list_peak_lists), key=operator.itemgetter(0), reverse=True)]
	file_lens = [thing[0] for thing in sorted_array]
	list_peak_lists = [thing[2] for thing in sorted_array]
	bed_name_list = [thing[1] for thing in sorted_array]
	all_peaks_list = []
	file_num = 0
	for peak_list in list_peak_lists:
		for line in peak_list:
			all_peaks_list.append([line[0]] + [int(line[1])] + [int(line[2])] + [str(file_num)]+line[3:])
		file_num += 1
	all_peaks_list = sorted(all_peaks_list, key=operator.itemgetter(0,1))
	for line_loop in range(len(all_peaks_list)):
		for item_loop in range(len(all_peaks_list[line_loop])):
			all_peaks_list[line_loop][item_loop] = str(all_peaks_list[line_loop][item_loop])
	if save_intermediate_file == True:
		write_bed(intermediate_filename, all_peaks_list)
	have_current_peak = None
	merged_list = []
	total_line_num = 0
	last_one = 0
	for peak in all_peaks_list:
		total_line_num += 1
		if have_current_peak == None:
			have_current_peak = peak[:3]+[[0]*bed_count]+peak[4:]
			have_current_peak[3][int(peak[3])] = 1
			continue
		else:
			#changed this line, original version only checked if the base pair location overlapped, not if peaks were on the same chromosome
			# if int(peak[1]) < int(have_current_peak[2]):
			if (int(peak[1]) < int(have_current_peak[2])) and (peak[0] == have_current_peak[0]):
				have_current_peak[3][int(peak[3])] = 1
				if int(peak[2]) > int(have_current_peak[2]):
					have_current_peak[2] = peak[2]
			else:
				last_one = total_line_num
				which_files = []
				num_files = 0
				for name_loop in range(len(bed_name_list)):
					if have_current_peak[3][name_loop] == 1:
						which_files.append(bed_name_list[name_loop])
						num_files += 1
				have_current_peak = have_current_peak[:3] + [str(num_files)] + ['_'.join(which_files)] + have_current_peak[4:]
				merged_list.append([str(thing) for thing in have_current_peak])
				have_current_peak = peak[:3]+[[0]*bed_count]+peak[4:]
				have_current_peak[3][int(peak[3])] = 1
	write_bed(final_output, merged_list)

def clean_up_for_igv(final_output, cleaned_for_igv):
	with open(final_output, 'r') as textfile:
		with open(cleaned_for_igv, 'w') as writefile:
			for line in textfile:
				line = line.rstrip('\n').split('\t')[:4]
				thing = line
				print('\t'.join(thing), file=writefile)


def pick_peaks_high_enough_number(input_file, save_file, col_num, my_num):
	my_peaklist = []
	with open(input_file, 'r') as textfile:
		for line in textfile:
			line = line.strip('\n').split('\t')
			my_peaklist.append(line)
	best_peaks = []
	for line in my_peaklist:
		if int(line[col_num]) >= int(my_num):
			best_peaks.append(line)
	write_bed(save_file, best_peaks)

#you can put an absolute path for any file location if you want, it shouldn't have any problems
#path to directory with the beds
bed_dir = os.path.expanduser('')
#an intermediate file for bugchecking, won't save if 'save_intermediate_file' is false
intermediate_filename = 'test_file.txt'
#this is where the output bed is saved
final_output = 'file.bed'
#this is where the cleaned up bed is saved (just has chr_number, start, end, and number_of_files for each peak)
cleaned_for_igv = 'cleaned.bed'
best_peaks_file = 'consensus_peaks.bed'
save_intermediate_file = False
dont_touch = [intermediate_filename, final_output, cleaned_for_igv, best_peaks_file]

# file_num = len(list(glob.iglob(os.path.join(bed_dir, '*'))))

merge_count(bed_dir, intermediate_filename, final_output, dont_touch, save_intermediate_file=save_intermediate_file)
clean_up_for_igv(final_output, cleaned_for_igv)
# python indexing begins at 0, so 3 = 4th column
pick_peaks_high_enough_number(final_output, best_peaks_file, 3, 4)
